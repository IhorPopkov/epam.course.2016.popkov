using System;

namespace TaskMatrix
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var firstArray = new int[6, 5]
                {
                    {4,3,5,-4,5},
                    {-2,2,1,3,3},
                    {8,3,-3,2,2},
                    {5,1,-1,3,4},
                    {3,-2,6,4,7},
                    {8,3,1,-1,9}
                };

                var secondArray = new int[4, 4]
                {
                    {4,3,5,-4},
                    {-2,2,1,3},
                    {8,3,-3,2},
                    {5,1,-1,3}
                };

                var firstMatrix = new Matrix(firstArray);
                var secondMatrix = new Matrix(secondArray);

                Console.WriteLine("\nMatrix #1\n{0}", firstMatrix);
                Console.WriteLine("\nMatrix #2\n{0}", secondMatrix);

                Console.WriteLine("First Matrix. Minor of 4: {0}", firstMatrix.Minor(4));
                Console.WriteLine("Second Matrix. Minor of (1,2): {0}", secondMatrix.GetMinor(1,2));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }
    }
}