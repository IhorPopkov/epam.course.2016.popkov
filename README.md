# README #

* The solution of each task is in a separate file.
* Each task has two files "<Task>.cs" and "Program.cs"
* To run task you must enter thes commands:

###Matrix###
* cd Matrix
* csc /target:library Matrix.cs
* csc /r:Matrix.dll Program.cs

###Polynomial###
* cd Polynomial
* csc /target:library Polynomial.cs
* csc /r:Polynomial.dll Program.cs

###Vector###
* cd Vector
* csc /target:library Vector.cs
* csc /r:Vector.dll Program.cs

###Rectangle###
* cd Rectangle
* csc /target:library Rectangle.cs
* csc /r:Rectangle.dll Program.cs