using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace TaskVector
{
    public enum Components : int
    {
        Zero = 0,
        One = 1,
        Two = 2
    }

    public class Vector
    {
        public List<Components> Plural {get; private set;}
        
        public Vector(List<Components> list)
        {
            Plural = list;
        }

        public static bool IsOrthogonal(Vector one, Vector two)
        {
            if(one.Plural.Count != two.Plural.Count) 
            {
                throw new Exception("Vectors have different dimension");
            }
            int result = 0;

            for (int i = 0; i < one.Plural.Count; i++)
            {
                result += (int)one.Plural[i] * (int)two.Plural[i];                
            }
            if (result == 0)
            {
                return true;
            }
            return false;
        }

        public int CountOfTwo()
        {
            var sum =   from c in Plural 
                        where c == Components.Two 
                        select c; 
            return sum.Count();
        }

        public static Vector Cross(Vector one, Vector two)
        {
            if(one.Plural.Count != two.Plural.Count) throw new Exception("Vectors have different dimension");
            if(IsOrthogonal(one, two)) throw new Exception("Vectors are Orthogonal");

            List<Components> newList = new List<Components>();
            for(int i = 0; i < one.Plural.Count; i++)
            {
                newList.Add(CharacterCross(one.Plural[i], two.Plural[i]));
            }
            return new Vector(newList);
        }

        private static Components CharacterCross(Components one, Components two)
        {
            if(one == Components.Zero || two == Components.Zero)
            {
                return Components.Zero;
            }
            else if(one == Components.One || two == Components.One)
            {
                return Components.One;
            }            
            return Components.Two;           
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            str.Append("( ");
            foreach (var item in Plural)
            {
                str.Append(string.Format("{0} ", (int)item));
            }
            str.Append(")");
            return str.ToString();
        }
    }
}