using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskVector
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                List<int> firstArray = new List<int> { 1, 2, 0, 2, 1, 0, 2, 1 };
                List<int> secondArray = new List<int> { 0, 1, 0, 1, 2, 0, 0, 2 };

                var firstVector = new Vector(firstArray.Cast<Components>().ToList<Components>());
                var secondVector = new Vector(secondArray.Cast<Components>().ToList<Components>());

                Console.WriteLine("\nFirst Vector: {0}", firstVector);
                Console.WriteLine("\nSecond Vector: {0}", secondVector);
                Console.WriteLine("\nFirst and Second Vector are Ortogonal: {0}", Vector.IsOrthogonal(firstVector, secondVector));
                Console.WriteLine("\nFirst Vector has {0} values 2", firstVector.CountOfTwo());
                Console.WriteLine("Cross of First and Second Vectors: {0}", Vector.Cross(firstVector, secondVector));
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}