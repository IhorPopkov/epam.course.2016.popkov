using System;

namespace TaskRectangule
{
    public class Program
    {
        static void Test()
        {
            // Instanciating
            Console.WriteLine("Creation of another Rectangle instance (rect1)");
            Rectangle rect1 = new Rectangle(new Point(0,0), new Point(8,8));
            Console.WriteLine(rect1);
            Console.WriteLine();

            Console.WriteLine("Creation of Rectangle instance (rect2)");
            Rectangle rect2 = new Rectangle(new Point(6, 7), new Point(16, 17));
            Console.WriteLine(rect2);
            Console.WriteLine();

            Console.WriteLine($"Test intersection for Rectangles \n{rect1}\n, {rect2}\n");
            Console.WriteLine(Rectangle.Cross(rect1, rect2));
            Console.WriteLine();

            //Changing size and moving
            Console.WriteLine("Moving rect2 (Moving vector is (1,2) )");
            rect2.Move(1, Axis.X);
            rect2.Move(2, Axis.Y);
            Console.WriteLine(rect2);
            Console.WriteLine();

            Console.WriteLine("Changing size of rect1 (new coordinates of running point are (10,10))");
            rect1.ChangeSize(new Point(1,1), new Point(9,9));
            Console.WriteLine(rect1);
            Console.WriteLine();

            Rectangle rect1_intersection_not_exists = new Rectangle(0, 0, 8, 8);
            Rectangle rect2_intersection_not_exists = new Rectangle(9, 0, 10, 10);
            //TODO: in this case your app will crash
            try
            {
                Console.WriteLine($"Test intersection not exists for Rectangles \n{rect1_intersection_not_exists}, \n\n{rect2_intersection_not_exists}");
                Console.WriteLine(Rectangle.Cross(rect1_intersection_not_exists, rect2_intersection_not_exists));
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error, {e.Message}\n");
            }

            Rectangle rect1_original = new Rectangle(0, 0, 8, 8);
            Rectangle rect2_original = new Rectangle(6, 7, 16, 17);
            Console.WriteLine($"Container for Rectangles\n{rect1_original},\n{rect2_original}\n");
            Console.WriteLine(Rectangle.Merge(rect1_original, rect2_original));
            Console.WriteLine();

            //Delay
            Console.ReadKey();
        }

        public static void Main(string[] args)
        {
            Test();
            //TODO: there are single big try-catch block, it is wrong because if some exceptions happen program will stop. You need try-catch block per each method
            try
            {
                var firstRectangule = new Rectangle(new Point(1, 2), new Point(7, 5));
                var secondRectangule = new Rectangle(new Point(5, 1), new Point(9, 6));

                Console.WriteLine("\nFirst Rectangule\n {0}", firstRectangule);
                Console.WriteLine("\nSecond Rectangule\n {0}", secondRectangule);

                var thirdRectangle = Rectangle.Merge(firstRectangule, secondRectangule);
                var fourthRectangle = Rectangle.Cross(firstRectangule, secondRectangule);

                Console.WriteLine("\nMerged Rectangule from first and second\n {0}", thirdRectangle);
                Console.WriteLine("\nCross of first and second Rectangules\n {0}", fourthRectangle);

                firstRectangule.Move(-5, Axis.X);
                Console.WriteLine("\nMove first Rectangule on X axis on (-5) values\n {0}", firstRectangule);

                secondRectangule.Move(3, Axis.Y);
                Console.WriteLine("\nMove second Rectangule on Y axis on (3) values\n {0}", secondRectangule); 

                firstRectangule.ChangeSize(rightUp: new Point(5,5));  
                Console.WriteLine("\nChange size of first Rectangule\n {0}", firstRectangule);

                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}