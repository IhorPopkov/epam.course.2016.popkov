using System;
using System.Text;
using System.Linq;

namespace TaskMatrix
{
    public class Matrix
    {
        private int Rows 
        {
            get {
                return _matrixArray.GetLength(0);
            }
        }
        private int Columns 
        {
            get {
                return _matrixArray.GetLength(1);
            }
        }
        //TODO: int? It is too much sampled. Need real number (double or float type)
        private int[,] _matrixArray;


        public Matrix(int[,] matrixArray)
        {
            _matrixArray = matrixArray;
        }

        public Matrix(int rows, int columns)
        {
            _matrixArray = new int[rows, columns];
            var randomNumber = new Random();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    _matrixArray[i, j] = randomNumber.Next(100);
                }
            }
        }

        public Matrix(int size) : this(size, size)
        { }

        public int Minor(int number)
        {
            if (number > Math.Min(Rows, Columns))
            {
                //TODO: why? Message is not sufficient. Explain reason.
                throw new Exception("Can not get Minor");
            }
            var rand = new Random();
            int[] rows = new int[number];
            int[] columns = new int[number];
            int tmp;

            for (int i = 0; i < number; i++)
            {
                do
                {
                    tmp = rand.Next(0, Rows);
                }
                while (rows.Contains(tmp));
                rows[i] = tmp;

                do
                {
                    tmp = rand.Next(0, Columns);
                }
                while (columns.Contains(tmp));
                columns[i] = tmp;
            }

            var array = new int[number, number];
            for (int i = 0; i < number; i++)
            {
                for (int j = 0; j < number; j++)
                {
                    array[i, j] = _matrixArray[rows[i], columns[j]];
                }
            }
            
            //TODO: unused
            var newMatrix = new Matrix(array);

            return Determ(array);
        }

        // public int AdditionMinor(int number)
        // {
        //     if (Rows != Columns) throw new Exception("The number of rows in the matrix is not equal to the number of columns");
        //     if (number > Columns) throw new Exception("Can not get Minor");

        //     var rand = new Random();
        //     int[] rows = new int[number];
        //     int[] columns = new int[number];
        //     int tmp;

        //     for (int i = 0; i < number; i++)
        //     {
        //         do
        //         {
        //             tmp = rand.Next(0, Rows);
        //         }
        //         while (rows.Contains(tmp));
        //         rows[i] = tmp;

        //         do
        //         {
        //             tmp = rand.Next(0, Columns);
        //         }
        //         while (columns.Contains(tmp));
        //         columns[i] = tmp;
        //     }

        //     var array = new int[number, number];
        //     for (int i = 0; i < number; i++)
        //     {
        //         for (int j = 0; j < number; j++)
        //         {
        //             array[i, j] = _matrixArray[rows[i], columns[j]];
        //         }
        //     }

        //     var newMatrix = new Matrix(array);

        //     return Determ(array);
        // }

        //TODO: matrix parameter is redundant, you can use own instance of current class (this)
        private int[,] GetMinorMatrix(int[,] matrix, int row, int column)
        {
            //TODO: string message duplicating and can be put to some private variable. Or better to create some custom exception inherited from Exception.
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new Exception("The number of rows in the matrix is not equal to the number of columns");
            int[,] buf = new int[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if ((i != row) || (j != column))
                    {
                        //TODO: duplicating conditions. Can use nested "if"
                        if (i > row && j < column) buf[i - 1, j] = matrix[i, j];
                        if (i < row && j > column) buf[i, j - 1] = matrix[i, j];
                        if (i > row && j > column) buf[i - 1, j - 1] = matrix[i, j];
                        if (i < row && j < column) buf[i, j] = matrix[i, j];
                    }
                }
            return buf;
        }

        //TODO: Where is the summary or at least comments to the methods?
        public int GetMinor(int row, int column)
        {
            //TODO: string message duplicating and can be put to some private variable.
            if (Rows != Columns) throw new Exception("The number of rows in the matrix is not equal to the number of columns");
            return Determ(GetMinorMatrix(_matrixArray, row, column));
        }

        private int Determ(int[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new Exception("The number of rows in the matrix is not equal to the number of columns");
            int det = 0;
            //TODO: code conventions violation
            int Rank = matrix.GetLength(0);
            if (Rank == 1) det = matrix[0, 0];
            if (Rank == 2) det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            if (Rank > 2)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    det += (int)Math.Pow(-1, 0 + j) * matrix[0, j] * Determ(GetMinorMatrix(matrix, 0, j));
                }
            }
            return det;
        }

        public override string ToString()
        {
            var outMatrix = new StringBuilder();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    outMatrix.Append(_matrixArray[i, j] + "\t");
                }
                outMatrix.Append("\n");
            }
            return outMatrix.ToString();
        }
    }
}