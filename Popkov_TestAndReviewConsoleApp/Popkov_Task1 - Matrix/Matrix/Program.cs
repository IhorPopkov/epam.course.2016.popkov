using System;
using System.Diagnostics;

namespace TaskMatrix
{
    public class Program
    {
        static void Test()
        {
            Matrix m = new Matrix(new[,]
           {
            {5, 3, 1, 4},
            {1, 2, 3, -2},
            {0, 9, 8, 3},
            {7, -1, -3, 6}
           });
            Console.WriteLine(m);
            Stopwatch w = new Stopwatch();
            w.Start();
            //TODO: the function works too long (stucks), it should return -182
            var det = m.Minor(4);
            w.Stop();
            Console.WriteLine($"Determinant of matrix: {det}");
            Console.WriteLine($"time: {w.ElapsedTicks}");
            Console.WriteLine($"minor of element (1,2): {m.GetMinor(1, 2)}");
            //TODO: the function not works, it returns 19, should be -70
            Console.WriteLine($"minor of 3 degree: {m.Minor(3)}");
            //TODO: missing "�������������� ���� �� ����� �-�� �������"
            // Console.WriteLine($"supplemented minor to minor of 3 degree: {m.AdjunctMinor(3)}");
            //TODO: missing "����������� ����� ������� �������� (���������, ��������, ������) ��� �������"
            Console.ReadKey();
        }

        public static void Main(string[] args)
        {
            Test();
            try
            {
                var firstArray = new int[6, 5]
                {
                    {4,3,5,-4,5},
                    {-2,2,1,3,3},
                    {8,3,-3,2,2},
                    {5,1,-1,3,4},
                    {3,-2,6,4,7},
                    {8,3,1,-1,9}
                };

                var secondArray = new int[4, 4]
                {
                    {4,3,5,-4},
                    {-2,2,1,3},
                    {8,3,-3,2},
                    {5,1,-1,3}
                };

                var firstMatrix = new Matrix(firstArray);
                var secondMatrix = new Matrix(secondArray);

                Console.WriteLine("\nMatrix #1\n{0}", firstMatrix);
                Console.WriteLine("\nMatrix #2\n{0}", secondMatrix);

                Console.WriteLine("First Matrix. Minor of 4: {0}", firstMatrix.Minor(4));
                Console.WriteLine("Second Matrix. Minor of (1,2): {0}", secondMatrix.GetMinor(1,2));
            }

            //TODO: It should be caught separately, if some method throws then just skip it and continue to execure others
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //TODO: Window dissapears. ReadKey() missing 
            Console.ReadKey();
        }
    }
}