using System;
using System.Text;
using System.Collections.Generic;

namespace TaskPolynomial
{
    public class Polynomial
    {
        private List<int> Coefficients;
        public int Power 
        {
            get {
                return Coefficients.Count;
            }
        } 
        public Polynomial(List<int> сoefficients)
        {
            Coefficients = сoefficients;
        }

        public int Determine(int argument)
        {
            int sum = 0;
            for (int i = 0; i < Coefficients.Count; i++)
            {
                sum += (int)Math.Pow(argument,i) * Coefficients[i];
            }
            return sum;
        }

        public static Polynomial operator+ (Polynomial left, Polynomial right)
        {
            var newList = new List<int>();
            while (left.Coefficients.Count < right.Coefficients.Count)
            {
            	left.Coefficients.Add(0);
            }
            while (left.Coefficients.Count > right.Coefficients.Count)
            {
                right.Coefficients.Add(0);
            }           
            for (int i = 0; i < left.Coefficients.Count; i++)
            {
                newList.Add(left.Coefficients[i] + right.Coefficients[i]);
            }
            return new Polynomial(newList);
        }

        public static Polynomial operator- (Polynomial left, Polynomial right)
        {
            var newList = new List<int>();
            while (left.Coefficients.Count < right.Coefficients.Count)
            {
            	left.Coefficients.Add(0);
            }
            while (left.Coefficients.Count > right.Coefficients.Count)
            {
                right.Coefficients.Add(0);
            }           
            for (int i = 0; i < left.Coefficients.Count; i++)
            {
                newList.Add(left.Coefficients[i] - right.Coefficients[i]);
            }
            return new Polynomial(newList);
        }

        public static Polynomial operator* (Polynomial left, Polynomial right)
        {
            var newList = new List<int>();
            int count = left.Coefficients.Count + right.Coefficients.Count - 1;
            for (int i = 0; i < count; i++)
            {
                newList.Add(0);
            }
            for (int i = 0; i < left.Coefficients.Count; i++)
            {
                for (int j = 0; j < right.Coefficients.Count; j++)
                {
                    newList[i+j] += left.Coefficients[i] * right.Coefficients[j];
                }
            }
            return new Polynomial(newList);
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            str.Append(Coefficients[0]);
            for (int i = 1; i < Coefficients.Count; i++)
            {
                str.Append(String.Format(" + ({0} x^{1})", Coefficients[i], i));
            }
            return str.ToString();
        }
    }
}