using System.Collections.Generic;
using System;

namespace TaskPolynomial
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var firstList = new List<int> { 4, 3, -2, -6, 2, 7, 8 };
                var secondList = new List<int> { 5, 2, 7, 1, 5, 4, 2 };
                var thirdList = new List<int> { 2, -3, 4 };

                var firstPolynomial = new Polynomial(firstList);
                var secondPolynomial = new Polynomial(secondList);
                var thirdPolynomial = new Polynomial(thirdList);

                Console.WriteLine("\nFirst Polynomial: {0}", firstPolynomial);
                Console.WriteLine("\nSecond Polynomial: {0}", secondPolynomial);
                Console.WriteLine("\nThird Polynomial: {0}", thirdPolynomial);

                Console.WriteLine("\nThird Polynomial argument 3: {0}", thirdPolynomial.Determine(3));
                Console.WriteLine("\nFirst + Second = {0}", firstPolynomial + secondPolynomial);
                Console.WriteLine("\nFirst - Second = {0}", firstPolynomial - secondPolynomial);
                Console.WriteLine("\nFirst * Third = {0}", firstPolynomial * thirdPolynomial);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}