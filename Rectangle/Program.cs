using System;

namespace TaskRectangule
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var firstRectangule = new Rectangle(new Point(1, 2), new Point(7, 5));
                var secondRectangule = new Rectangle(new Point(5, 1), new Point(9, 6));

                Console.WriteLine("\nFirst Rectangule\n {0}", firstRectangule);
                Console.WriteLine("\nSecond Rectangule\n {0}", secondRectangule);

                var thirdRectangle = Rectangle.Merge(firstRectangule, secondRectangule);
                var fourthRectangle = Rectangle.Cross(firstRectangule, secondRectangule);

                Console.WriteLine("\nMerged Rectangule from first and second\n {0}", thirdRectangle);
                Console.WriteLine("\nCross of first and second Rectangules\n {0}", fourthRectangle);

                firstRectangule.Move(-5, Axis.X);
                Console.WriteLine("\nMove first Rectangule on X axis on (-5) values\n {0}", firstRectangule);

                secondRectangule.Move(3, Axis.Y);
                Console.WriteLine("\nMove second Rectangule on Y axis on (3) values\n {0}", secondRectangule); 

                firstRectangule.ChangeSize(rightUp: new Point(5,5));  
                Console.WriteLine("\nChange size of first Rectangule\n {0}", firstRectangule);              
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}