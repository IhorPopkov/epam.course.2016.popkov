using System;
using System.Text;

namespace TaskRectangule
{
    public class Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void AddToX(int offset)
        {
            X += offset;
        }

        public void AddToY(int offset)
        {
            Y += offset;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var newPoint = (Point)obj;
            if (X == newPoint.X && Y == newPoint.Y)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return X + Y;
        }

        public static bool Bigger(Point one, Point two)
        {
            if (one.X > two.X && one.Y > two.Y)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }

    public enum Axis
    {
        X,
        Y
    }

    public class Rectangle
    {
        public Point LeftDown { get; private set; }
        public Point LeftUp { get; private set; }
        public Point RightDown { get; private set; }
        public Point RightUp { get; private set; }

        public Rectangle(int x1, int y1, int x2, int y2) : this(new Point(x1, y1), new Point(x2, y2))
        { }
        public Rectangle(Point firstPoint, Point secondPoint)
        {
            if (!isRectangle(firstPoint, secondPoint))
            {
                throw new Exception("It is not Rectangle");
                //TODO create new exception
            }
            if (Point.Bigger(firstPoint, secondPoint))
            {
                this.RightUp = firstPoint;
                this.LeftDown = secondPoint;
            }
            else
            {
                this.RightUp = secondPoint;
                this.LeftDown = firstPoint;
            }
            FindSecondaryPoints();
        }

        private static bool isRectangle(Point one, Point two)
        {
            if (one.Equals(two) || one.X == two.X || one.Y == two.Y)
            {
                return false;
            }
            return true;
        }

        public void Move(int offset, Axis axis)
        {
            if (axis == Axis.X)
            {
                LeftDown.AddToX(offset);
                RightDown.AddToX(offset);
                LeftUp.AddToX(offset);
                RightUp.AddToX(offset);
            }
            if (axis == Axis.Y)
            {
                LeftDown.AddToY(offset);
                RightDown.AddToY(offset);
                LeftUp.AddToY(offset);
                RightUp.AddToY(offset);
            }
        }

        public static Rectangle Merge(Rectangle one, Rectangle two)
        {
            Point leftDown = new Point(Math.Min(one.LeftDown.X, two.LeftDown.X), Math.Min(one.LeftDown.Y, two.LeftDown.Y));
            Point rightUp = new Point(Math.Max(one.RightUp.X, two.RightUp.X), Math.Max(one.RightUp.Y, two.RightUp.Y));
            return new Rectangle(leftDown, rightUp);
        }

        public static Rectangle Cross(Rectangle one, Rectangle two)
        {
            if (one.IsCrossing(two))
            {
                Point leftDown = new Point(Math.Max(one.LeftDown.X, two.LeftDown.X), Math.Max(one.LeftDown.Y, two.LeftDown.Y));
                Point rightUp = new Point(Math.Min(one.RightUp.X, two.RightUp.X), Math.Min(one.RightUp.Y, two.RightUp.Y));
                return new Rectangle(leftDown, rightUp);
            }
            else
            {
                throw new Exception("Rectangles disjoint");
                //TODO create new exception
            }
        }

        public void ChangeSize(Point leftDown = null, Point rightUp = null)
        {
            if (leftDown != null && rightUp != null)
            {
                if (!isRectangle(leftDown, rightUp))
                {
                    throw new Exception("It is not Rectangle");
                    //TODO create new exception
                }
                LeftDown = leftDown;
                RightUp = rightUp;
            }

            if (leftDown != null)
            {
                if (!isRectangle(leftDown, RightUp))
                {
                    throw new Exception("It is not Rectangle");
                    //TODO create new exception
                }
                LeftDown = leftDown;
            }

            if (rightUp != null)
            {
                if (!isRectangle(LeftDown, rightUp))
                {
                    throw new Exception("It is not Rectangle");
                    //TODO create new exception
                }
                RightUp = rightUp;
            }
            FindSecondaryPoints();
        }

        private void FindSecondaryPoints()
        {
            this.LeftUp = new Point(LeftDown.X, RightUp.Y);
            this.RightDown = new Point(RightUp.X, LeftDown.Y);
        }

        public override string ToString()
        {
            return string.Format("{0} {1}\n{2} {3}", LeftUp, RightUp, LeftDown, RightDown);
        }

        private bool IsCrossing(Rectangle rect)
        {
            return (LeftDown.X < rect.RightUp.X && RightUp.X > rect.LeftDown.X
                && LeftDown.Y < rect.RightUp.Y && RightUp.Y > rect.LeftDown.Y);
        }
    }
}